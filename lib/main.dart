import 'package:flutter/material.dart';
import 'package:my_category_widget/category.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  final List<String> titles = [
    'Californication',
    'Shameless',
    'Expanse',
    'Taboo',
    'Game of Thrones',
    'Incorpotated',
    'Film1',
    'Film2',
    'Film3',
    'Film4'
  ];
  final List<IconData> icons = [
    Icons.cake,
    Icons.accessibility,
    Icons.account_balance,
    Icons.account_circle,
    Icons.adb,
    Icons.airline_seat_recline_extra,
    Icons.account_balance,
    Icons.account_circle,
    Icons.adb,
    Icons.airline_seat_recline_extra,
  ];

  @override
  Widget build(BuildContext buildContext) {
    return MaterialApp(
        debugShowCheckedModeBanner: false, home: CategoryPage(titles, icons));
  }
}
