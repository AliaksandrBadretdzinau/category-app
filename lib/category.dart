import 'package:flutter/material.dart';
import 'package:quiver/iterables.dart';

class CategoryPage extends StatelessWidget {
  final List<String> titles;
  final List<IconData> icons;

  CategoryPage(this.titles, this.icons);

  @override
  Widget build(BuildContext buildContext) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            'Favourite Serials',
            style: TextStyle(color: Colors.black87),
          ),
          backgroundColor: Colors.green[100],
        ),
        backgroundColor: Colors.green[100],
        body: Category(titles, icons));
  }
}

class Category extends StatelessWidget {
  final List<String> titles;
  final List<IconData> icons;

  Category(this.titles, this.icons);

  @override
  Widget build(BuildContext buildContext) {
    if (_isPortraitOrientation(buildContext)) {
      return _buildPortrait(buildContext);
    } else {
      return _buildLandscape(buildContext);
    }
    // return Container(
    //   color: Colors.transparent,
    //   width: 300,
    //   height: 1000,
    //   padding: EdgeInsets.all(40),
    //   child: Column(
    //     children: buildRows(titles, icons),
    //   ),
    //   alignment: Alignment.topCenter,
    // );
  }

  Widget _buildLandscape(BuildContext buildContext) {
    return GridView.count(
      crossAxisCount: 2,
      childAspectRatio: 3,
      children: buildRows(titles, icons),
      padding: EdgeInsets.all(20),
    );
  }

  Widget _buildPortrait(BuildContext buildContext) {
    return ListView.builder(
      itemBuilder: (BuildContext context, int index) =>
          buildRow(titles[index], icons[index]),
      itemCount: titles.length,
    );
  }

  _isPortraitOrientation(context) {
    return MediaQuery.of(context).orientation == Orientation.portrait;
  }
}

final buildRows = (List<String> titles, List<IconData> icons) =>
    zip([titles, icons]).map((e) => buildRow(e[0], e[1])).toList();

Widget buildRow(String title, IconData icon) {
  return InkWell(
    highlightColor: Colors.green[300],
    splashColor: Colors.green[300],
    borderRadius: BorderRadius.circular(20.0),
    child: Padding(
        padding: EdgeInsets.all(30),
        child: Row(children: [
          Icon(
            icon,
            size: 55,
          ),
          Padding(
            padding: EdgeInsets.only(left: 15),
            child: Text(
              title,
              style: TextStyle(fontSize: 22),
            ),
          ),
        ])),
    onTap: () => print('Tapped!'),
  );
}
